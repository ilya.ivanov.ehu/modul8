public class Main {
    public static void main(String[] args) throws InstantiationException {
        demonstrateTest1();
        demonstrateTriangle();
        demonstrateCounter();
        demonstrateTime();

    }
    public static void demonstrateTest1() {
        Task1 testClass1 = new Task1();

        testClass1.setFirstVariable(9);
        testClass1.setSecondVariable(93);


        int largestVariable = testClass1.findLargestVariable();
        System.out.printf("The largest variable of the class = %s\n", largestVariable);
        int sumOfVariables = testClass1.calculateSumOfAttributes();
        System.out.printf("The sum of class variables = %s\n", sumOfVariables);
    }
    public static void demonstrateTriangle() throws InstantiationException {
         Triangle  newTriangle = new  Triangle(20, 32, 45);

        System.out.printf("\n%s", newTriangle);

        double trianglePerimeter = newTriangle.calculatePerimeter();
        System.out.printf("Perimeter = %s\n", trianglePerimeter);

        double triangleArea = newTriangle.calculateArea();
        System.out.printf("Area = %s\n", triangleArea);
    }
    public static void demonstrateCounter () throws InstantiationException {
        System.out.println("Construct 'counter' with default value 0 and range[0;5]" );
        var counter = new DecimalCounter();

        System.out.println("Counter current value is " + counter.value() );
        System.out.println("Increase counter 10 times:");
        for (int i = 0; i < 10; i++) {
            counter.increase();
            System.out.println(i + 1 + ": " + counter.value());
        }

        System.out.println("Construct 'counter2' with value 3 and range[0;4]" );
        var counter2 = new DecimalCounter(0, 4, 3);

        System.out.println("Counter2 current value is " + counter2.value() );
        System.out.println("Decrease counter 5 times:");
        for (int i = 0; i < 5; i++) {
            counter2.decrease();
            System.out.println(i + 1 + ": " + counter2.value());
        }
    }
    public static void demonstrateTime() {
        MyTime time1 = new MyTime(12, 5, 70);

        System.out.printf("\nTime1 = %s\n", time1.showTime());

        time1.setHour(22);
        time1.setMinute(31);
        time1.setSecond(59);

        System.out.printf("Time1 set to -> %s\n", time1.showTime());

        time1.setTime(2, 14, 8);
        System.out.printf("Time1 set to -> %s\n", time1.showTime());

        time1.addMinutes(46);
        System.out.printf("Added 46 minutes to Time1 -> %s\n", time1.showTime());

        MyTime time2 = new MyTime(2, 59, 51);
        time1.add(time2);

        System.out.printf("Added %s to Time1 -> %s\n", time2.showTime(), time1.showTime());

        MyTime time3 = new MyTime(23, 0, 0);

        time1.subtract(time3);
        System.out.printf("Subtracted %s from Time1 -> %s\n", time3.showTime(), time1.showTime());

    }
}











