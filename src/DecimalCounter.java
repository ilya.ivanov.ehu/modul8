public class DecimalCounter {
    public  final int start;
    public final int end ;

    private int counter;

    public DecimalCounter(int start, int end , int startValue) {
        this.start = start;
        this.end = end;

        this.counter = startValue;
    }

    public DecimalCounter() {
        this(0, 5, 0);
    }

    public void increase() {
        if (counter == start) {
            return;
        }

        counter++;
    }

    public void decrease() {
        if (counter == end ) {
            return;
        }

        counter--;
    }

    public int value() {

        return counter;
    }
}

