public class Triangle{
    private double sideA;
    private double sideB;
    private double sideC;
    public Triangle(double a, double b, double c) throws InstantiationException {
        boolean validTriangle = validateTriangle(a, b, c);
        if(!validTriangle) {
            throw new InstantiationException("Triangle inequality theorem violation! Check the sides values.");
        }
        this.sideA = a;
        this.sideB = b;
        this.sideC = c;
    }

    private static boolean validateTriangle(double a, double b, double c) {
        return (a + b) > c && (a + c) > b && (b + c) > a;
    }


    public double getSideA() {
        return sideA;
    }

    public void setSideA(double a) throws InstantiationException {
        boolean validTriangle = validateTriangle(a, this.sideB, this.sideC);
        if(!validTriangle) {
            throw new InstantiationException("Triangle inequality theorem violation! Check the sides values.");
        }
        this.sideA = a;
    }
    public double getSideB() {
        return sideB;
    }

    public void setSideB(double b) throws InstantiationException {
        boolean validTriangle = validateTriangle(this.sideA, b, this.sideC);
        if(!validTriangle) {
            throw new InstantiationException("Triangle inequality theorem violation! Check the sides values.");
        }
        this.sideB = b;
    }


    public double getSideC() {
        return sideC;
    }

    public void setSideC(double c) throws InstantiationException {
        boolean validTriangle = validateTriangle(this.sideA, this.sideB, c);
        if(!validTriangle) {
            throw new InstantiationException("Triangle inequality theorem violation! Check the sides values.");
        }
        this.sideC = c;
    }


    public String toString() {
        return String.format("Triangle{sideA=%s, sideB=%s, sideC=%s}\n", sideA, sideB, sideC);
    }

    public double calculateArea() {
        double semiPerimeter = calculatePerimeter() / 2;
        return Math.sqrt(semiPerimeter * (semiPerimeter - sideA) * (semiPerimeter - sideB) * (semiPerimeter - sideC));
    }

    public double calculatePerimeter() {
        return sideA + sideB + sideC;
    }
}




