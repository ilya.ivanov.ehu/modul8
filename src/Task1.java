public class  Task1 {

    private int firstVariable;
    private int secondVariable;

    public Task1() {
        this.firstVariable = 0;
        this.secondVariable = 0;
    }

    public Task1(int firstVariable, int secondVariable) {
        this.firstVariable = firstVariable;
        this.secondVariable = secondVariable;
    }

    public int getFirstVariable() {
        return firstVariable;
    }

    public void setFirstVariable(int firstVariable) {
        this.firstVariable = firstVariable;
    }

    public int getSecondVariable() {
        return secondVariable;
    }

    public void setSecondVariable(int secondVariable) {
        this.secondVariable = secondVariable;
    }

    @Override
    public String toString() {
        return String.format("Test1{firstVariable=%s, secondVariable=%s}", firstVariable, secondVariable);
    }

    public int calculateSumOfAttributes() {
        return firstVariable + secondVariable;
    }

    public int findLargestVariable() {
        if(firstVariable > secondVariable) {
            return firstVariable;
        }
        if(firstVariable == secondVariable) {
            System.out.println("Class variables are equal.");
            return 0;
        }
        return secondVariable;
    }
}

