
    public class Task2 {

        private int firstVariable;
        private int secondVariable;

        public Task2() {
            this.firstVariable = 0;
            this.secondVariable = 0;
        }

        public Task2(int firstVariable, int secondVariable) {
            this.firstVariable = firstVariable;
            this.secondVariable = secondVariable;
        }

        public int getFirstVariable(){

            return this.firstVariable;
        }

        public void setFirstVariable(int firstVariable) {

            this.firstVariable = firstVariable;
        }

        public int getSecondVariable() {
            return this.secondVariable;
        }

        public void setSecondVariable(int secondVariable) {
            this.secondVariable = secondVariable;
        }
    }


